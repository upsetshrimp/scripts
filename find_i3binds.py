import os
import sys
HOME = os.path.expanduser("~")


def filter_keybinds(keybinds: list, filter: str):
    filtered = []
    for bind in keybinds:
        if filter in bind:
            filtered.append(bind)
    return filtered


def get_lines(file_path) -> list:
    f = open(file_path, "r")
    return f.readlines()


def find_bindsym(lines: list):
    in_mode = False
    keybinds = []
    for line in lines:
        if "mode" in line:
            in_mode = True
            continue
        if in_mode and "}" in line:
            in_mode = False
            continue
        if "bindsym" in line and \
                "#" not in line and \
                not in_mode:
            keybinds.append(line.strip("\n"))
    return keybinds


def get_filter():
    print(sys.argv[1])
    return sys.argv[1]


def print_lines(lines: list):
    for line in lines:
        print(line)


def main():
    i3conf_dir = f"{HOME}/.config/i3/config"
    keybinds = find_bindsym(get_lines(i3conf_dir))
    filtered = filter_keybinds(keybinds, get_filter())
    print_lines(filtered)


if __name__ == "__main__":
    main()
